import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom'

//dataprops from home is passed onto banner
// destructure para di mahabung code 
//export default function Banner(props){
export default function Banner({dataProp}){	
	const {title, content, destination, label}= dataProp
	//console.log(props)
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{title} </h1>
				<p>{content}</p>
			{/* variant means to mess with the colors*/}
				{/*<Button variant="primary"> enroll Now!</Button>*/}
				<Link className="btn btn-primary" to={destination}>{label}</Link>
			</Col>
		</Row>

	)
}
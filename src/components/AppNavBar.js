// long method
// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';
//import {Container,Nav, Navbar, NavDropdown} from 'react-Bootstrap'

// Short method
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import {Link,NavLink} from 'react-router-dom';
export default function AppNavBar(){
	return(
		<Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to= "/">Home</Nav.Link>
            <Nav.Link as={NavLink} to= "/courses" >Courses</Nav.Link>
            <Nav.Link as={NavLink} to= "/login" >Login</Nav.Link>
            <Nav.Link as={NavLink} to= "/register" >Register</Nav.Link>
           {/* <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>*/}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

	)
}

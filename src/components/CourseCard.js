import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
//destructuring is done in the parameter to retrieve the courseprop inside the
//course card works as a functional compenent
export default function CourseCard({courseProp}){
   // console.log(props.courseProp);
//   console.log(courseProp)
   //destructuring further
   const {name, description, price} =courseProp;
   /* hook state- use the state hook for this component to be able to store its stat
   states are used to keep track of information related to individual components

   syntax:

   // const[getter,setter]=useState(initialGetterValue)*/
   const [count,setCount]=useState(0);
   // naming convention descriptiove si getter, setter is getter plus set
   // states are null when first mounted, na setset lang sa new value depending on what the state was set to via hardcoding
   const [seatCount,setSeatCount]=useState(30);
   // using the state hookk returns an array with the first element being a value and the second element as a function thats used to change the value of the first
  // console.log({useState(0)})
   // function that keeps track of the ernollees for a course.
   //by default javascript is synchronous as it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
   // the setter function for useStates are asynchronous,allowing it to execute separeetely from other codes in the program
   // the "setCount" Function is being executerd while the console.log is already being completed resulting in the console to be beind by one count.

   function enroll (){
     
            setSeatCount(seatCount-1);
            setCount(count+1);
       //     console.log('enrollees: '+count)
       // }
        //else{
          //  alert("No more seats")

        //}
   }
   // parang eventlisterner si useeffect,
   // but in this case the the trigger is when the state is changed
   // use effects happen (trigger the code block) upon mount or when the state is changed

   /*
    use effect sytaxxxxx
    useEffect(()) => {
        code to be executed
    }, [state(s) to monitor]


   */
   // count and seatCount will trigger if either of those states changes
   // if the array is blank, the code will be executed on component mount only

   // DO NOT THE leave ARRAY completely. might trigger infinite loop
   useEffect(() => {
    if(seatCount !==0)
    {
        console.log("cute")
    }

        else{
          alert("No more seats")
    }   
   }, [count,seatCount])
    return(
        <Row className="my-3">
            <Col xs={12}>
                <Card className="cardCourse p-1 m-2">
                    <Card.Body>
                        <Card.Title>
                            {name} 
                        </Card.Title>

                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        
                        <Card.Text>
                            {description}
                        </Card.Text>

                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        
                        <Card.Text>
                            {price}
                        </Card.Text>
                        <Card.Text>
                            Enrollees: {count}
                        </Card.Text>
                        
                        <Button variant="primary" onClick={enroll}> Enroll</Button>
                    </Card.Body>
                </Card>
                </Col>
                
        </Row>
    )
}
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// import bootsrap css
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
// const name = `John Smith`;
// // jsx format
// let element = <hi>Hello, {name}</hi>

// /*
// root.render()- allows to render display our react js elements and display in our html.
// */
// //jsx (javascropt + xml)
// // it is an extension of javascript that lets us create objects which will be then compiled and added as html elements.

// //create a user objec,
//   const user ={
//     firstName: 'jane',
//     lastName:'smith'
//   }

// // create a function that will use the user as a parameters
// function formatName(profile){
//   return profile.firstName+" "+ profile.lastName;
// }
// // h1 tag is an example of JSX
// //JSX allows us to create HTML elements and at the same time allows us to apply JavaScript code to these elements making it easy to write both HTML and JavaScript code in a single file as opposed to creating two separate files (One for HTML and another for JavaScript syntax).
//   element = <h1>Hello, {formatName(user)}</h1>
// root.render(element);
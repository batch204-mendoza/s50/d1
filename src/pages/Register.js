import{useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Register(){
	const [email,setEmail]= useState("");
	const [password1, setPassword1]= useState("");
	const [password2, setPassword2]= useState("");
	const [isActive, setIsActive]= useState(false);

useEffect(() =>{
	/*console.log(email)
	console.log(password1)
	console.log(password2)*/
	if ((email !== '' && password1 !=='' &&password2 !== '') &&(password1 === password2))
	{
		setIsActive(true)
	}else{
		setIsActive(false)
	}

	}, [email,password1,password2])

function registerUser(e){
	e.preventDefault()
	setEmail("")
	setPassword1("")
	setPassword2("")
	alert('thank you for registering');
}
	
/*
to properly change and save input values, we must implement two-way binding
we need to capture whatever use types in the input as they are typing menaing we need the inputs .value value to get the value, we capture the event, in this case on change. the target of the onChange event is the input, meaning we can get the.value*/

	return(
		<Form onSubmit={e => registerUser(e)} >
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter email"
				value= {email}
				onChange={e=>setEmail(e.target.value)}
				required
				/> 
				<Form.Text>
				We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter password"
				value= {password1}
				onChange={e=>setPassword1(e.target.value)}
				required
				/> 
				<Form.Text clasName="text-muted">
				
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Verify password"
				value= {password2}
				onChange={e=>setPassword2(e.target.value)}
				required
				/> 
				
			</Form.Group>
		{/* you can pull up anything in  a return as long as you right it in code*/}
			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			:
			<Button variant="primary" type="submit" id="submitBtn" disabled>
					Submit
				</Button>	
			}
			


		</Form>
	)
}

// single page app
//  npm install react-router-dom command in terminal to summon routing for spa

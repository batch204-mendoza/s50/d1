import{useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Login(){
	const [email,setEmail]= useState("");
	const [password, setPassword]= useState("");
	const [isActive, setIsActive]= useState(false);

useEffect(() =>{
	if (email !== '' && password !=='') 
	{
		setIsActive(true)
	}else{
		setIsActive(false)
	}

	}, [email,password,])

function loginUser(e){
	e.preventDefault()
	setEmail("")
	setPassword("")
	alert('logged in ka na');
}
	


	return(
		<Form onSubmit={e => loginUser(e)} >
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				type="email"
				placeholder="Enter email"
				value= {email}
				onChange={e=>setEmail(e.target.value)}
				required
				/> 
				<Form.Text>
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
				type="password"
				placeholder="Enter password"
				value= {password}
				onChange={e=>setPassword(e.target.value)}
				required
				/> 
				<Form.Text clasName="text-muted">
				
				</Form.Text>
			</Form.Group>

		
		
			{isActive ?
				<Button variant="primary" type="submit" id="submitBtn">
					Submit
				</Button>
			:
			<Button variant="primary" type="submit" id="submitBtn" disabled>
					Submit
				</Button>	
			}
			


		</Form>
	)
}
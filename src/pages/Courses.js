import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';
export default function Courses(){
	// check if mock data was captured
	
	console.log(courseData);
	console.log(courseData[0]);
// Props- shorthand for "property" since components are considered as objects in react.js
// Props is a way to pass data from a parent component  to a child component.
// it is synonymous to the function parameter
//this is referred to props drilling. passing from parent to child
	const courses =courseData.map(course =>{
		return(
			<CourseCard courseProp={course} key={course.id}/>)
	})
	return(
		<>
		<h1>Courses</h1>
		{courses}

		</>
		)
}
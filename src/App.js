
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
// as keyword gives an allias to a component upon import
import './App.css';
import AppNavBar from './components/AppNavBar';
import Courses from './pages/Courses'; 
import Home from './pages/Home';
import Register from './pages/Register'
import Login from './pages/Login'
import Error from './pages/Error'
// all other compenenets/ pages will be contained in our main component : <app/>
//<>..</> fragment which ensures that adjacent jsx elements will be rendered and avoid this error.



function App() {
  return (
    <Router>
      <>
      <AppNavBar/>
      <Container >
      <Switch>
            {/*exact makes the path specific so that you woont have mismatched links
            ussually pics top to be bottom on the most likely chosen url link*/}
        <Route exact path="/"component={Home}/>
        <Route exact path="/courses"component={Courses}/>
        <Route exact path="/register" component={Register}/>
        <Route exact path="/login" component={Login}/>
        <Route component ={Error}/>
        </Switch>
      </Container>
      </>
    </Router>
    

  );
}

export default App;
